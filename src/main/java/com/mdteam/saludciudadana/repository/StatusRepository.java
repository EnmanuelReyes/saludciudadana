package com.mdteam.saludciudadana.repository;

import com.mdteam.saludciudadana.entity.Status;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 11/12/2016
 * Time: 01:27 PM
 */
public interface StatusRepository extends CrudRepository<Status, Integer> {
}
