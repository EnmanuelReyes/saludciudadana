package com.mdteam.saludciudadana.repository;

import com.mdteam.saludciudadana.entity.Category;
import com.mdteam.saludciudadana.entity.ProductType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 10/12/2016
 * Time: 11:26 AM
 */
public interface CategoryRepository extends CrudRepository<Category, Integer>{
    List<Category> findByProductTypeId(Integer id);
}
