package com.mdteam.saludciudadana.repository;

import com.mdteam.saludciudadana.entity.ProductType;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 10/12/2016
 * Time: 11:22 AM
 */
public interface ProductTypeRepository extends CrudRepository<ProductType, Integer> {
}
