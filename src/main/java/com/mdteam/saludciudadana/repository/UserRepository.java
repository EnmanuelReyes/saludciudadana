package com.mdteam.saludciudadana.repository;

import com.mdteam.saludciudadana.entity.Citizen;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel Reyes
 * Date: 08-Dec-16
 * Time: 10:14 AM
 */
public interface UserRepository extends PagingAndSortingRepository<Citizen, Integer> {
}
