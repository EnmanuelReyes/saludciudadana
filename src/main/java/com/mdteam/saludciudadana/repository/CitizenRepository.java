package com.mdteam.saludciudadana.repository;

import com.mdteam.saludciudadana.entity.Citizen;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 10/12/2016
 * Time: 11:19 AM
 */
public interface CitizenRepository extends CrudRepository<Citizen, String> {

}
