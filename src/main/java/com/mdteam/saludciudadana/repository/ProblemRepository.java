package com.mdteam.saludciudadana.repository;

import com.mdteam.saludciudadana.entity.Category;
import com.mdteam.saludciudadana.entity.Problem;
import org.springframework.beans.factory.parsing.ProblemReporter;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 10/12/2016
 * Time: 02:38 PM
 */
public interface ProblemRepository extends CrudRepository<Problem,Integer>{
    List<Problem> findByProductTypeId(Integer id);

}
