package com.mdteam.saludciudadana.repository;

import com.mdteam.saludciudadana.entity.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 09/12/2016
 * Time: 10:13 PM
 */
public interface ReportRepository extends CrudRepository<Report, Integer> {
}
