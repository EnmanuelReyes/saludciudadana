package com.mdteam.saludciudadana.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.mdteam.saludciudadana.*")
public class SaludciudadanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SaludciudadanaApplication.class, args);
	}

}
