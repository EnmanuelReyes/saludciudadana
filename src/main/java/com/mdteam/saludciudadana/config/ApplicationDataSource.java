package com.mdteam.saludciudadana.config;

import com.mdteam.saludciudadana.entity.*;
import com.mdteam.saludciudadana.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel Reyes
 * Date: 26-Oct-16
 * Time: 3:20 PM
 */
@Configuration
@EnableJpaRepositories(basePackages = "com.mdteam.saludciudadana.repository")
@EntityScan(basePackages = "com.mdteam.saludciudadana.entity")
public class ApplicationDataSource {

    @Bean
    public CommandLineRunner initProductTypes(ProductTypeRepository productTypeRepository) {
        return (args) -> {
            productTypeRepository.save(new ProductType(1, "Medicos"));
            productTypeRepository.save(new ProductType(2, "Alimenticios"));
        };
    }

    @Bean
    public CommandLineRunner initStatuses(StatusRepository statusRepository) {
        return (args) -> {
            statusRepository.save(new Status(1, "Abierta"));
            statusRepository.save(new Status(2, "En Progreso"));
            statusRepository.save(new Status(3, "Cerrada"));
        };
    }


    @Bean
    public CommandLineRunner initCitizens(CitizenRepository citizenRepository) {
        return (args) -> {
            Calendar c = Calendar.getInstance();
            c.set(1992, 8, 12);
            citizenRepository.save(new Citizen("40224746053", "Enmanuel", "Reyes", c.getTime(), "Santo Domingo", "Dominicana", "Masculino", "Soltero", "Programador"));
        };
    }

    @Bean
    public CommandLineRunner initCategories(CategoryRepository categoryRepository) {
        return (args) -> {
            categoryRepository.save(new Category(1, "Nutrición", ProductType.MEDICO));
            categoryRepository.save(new Category(2, "Cuidado Bucal", ProductType.MEDICO));
            categoryRepository.save(new Category(3, "Bebes y mamas", ProductType.MEDICO));
            categoryRepository.save(new Category(4, "Cuidado Corporal", ProductType.MEDICO));
            categoryRepository.save(new Category(5, "Salud Sexual", ProductType.MEDICO));
            categoryRepository.save(new Category(6, "Productos Sanitarios", ProductType.MEDICO));
            categoryRepository.save(new Category(7, "Cuidado Capilar", ProductType.MEDICO));

            categoryRepository.save(new Category(8, "Bebidas", ProductType.ALIMENTICIO));
            categoryRepository.save(new Category(9, "Bebidas Alcohólicas", ProductType.ALIMENTICIO));
            categoryRepository.save(new Category(10, "Condimentos y especias", ProductType.ALIMENTICIO));
            categoryRepository.save(new Category(11, "Lacteos", ProductType.ALIMENTICIO));
            categoryRepository.save(new Category(12, "Enlatados", ProductType.ALIMENTICIO));
            categoryRepository.save(new Category(13, "Vida Saludable", ProductType.ALIMENTICIO));
            categoryRepository.save(new Category(14, "Miscelaneos", ProductType.ALIMENTICIO));
        };
    }

    @Bean
    public CommandLineRunner initProblems(ProblemRepository problemRepository) {
        return (args) -> {
            problemRepository.save(new Problem(1, "Nombre incorrecto", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(2, "Ingredientes no descritos", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(3, "Peso/volumen diferente al estipulado", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(4, "No describe instrucciones de uso", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(5, "El producto no posee identificacion", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(6, "No cuenta con registro sanitario", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(7, "El producto esta vencido", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(8, "No cuenta con pais de origen", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(9, "No especifica modo de uso", ProductType.ALIMENTICIO));
            problemRepository.save(new Problem(10, "Empresa no identificada", ProductType.ALIMENTICIO));

            problemRepository.save(new Problem(11, "Nombre incorrecto", ProductType.MEDICO));
            problemRepository.save(new Problem(12, "Fabricante mal", ProductType.MEDICO));
            problemRepository.save(new Problem(13, "Registro Sanitario", ProductType.MEDICO));
            problemRepository.save(new Problem(14, "Fecha de Vencimiento", ProductType.MEDICO));
            problemRepository.save(new Problem(15, "Lote", ProductType.MEDICO));


        };
    }

    @Bean
    public CommandLineRunner init(ReportRepository reportRepository) {
        return (args) -> {
//            reportRepository.save(new Report(1, new Citizen("40224746053"),new Category(1),null, new Date()));
        };
    }
}
