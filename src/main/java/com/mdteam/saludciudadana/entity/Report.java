package com.mdteam.saludciudadana.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 09/12/2016
 * Time: 09:25 PM
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "report")
public class Report {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    private Citizen citizen;
    @ManyToOne
    private Category category;
    @OneToMany(cascade = CascadeType.ALL)
    private List<ProblemImage> problemImages;
    private Date date;
    private String note;
    private String productName;
    private String brand;
    private String volume;
    private String lote;
    private String uniqueId;
    private String regSan;
    private String regInd;
    private String commerceName;
    private String commerceRnc;
    private Double lat;
    private Double lng;
    private String commerceAddress;
    @ManyToOne
    private Status status;


    public Report() {
    }

    public Report(Integer id, Citizen citizen, Category category, List<ProblemImage> problemImages, Date date) {
        this.id = id;
        this.citizen = citizen;
        this.category = category;
        this.problemImages = problemImages;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Citizen getCitizen() {
        return citizen;
    }

    public void setCitizen(Citizen citizen) {
        this.citizen = citizen;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<ProblemImage> getProblemImages() {
        return problemImages;
    }

    public void setProblemImages(List<ProblemImage> problemImages) {
        this.problemImages = problemImages;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRegSan() {
        return regSan;
    }

    public void setRegSan(String regSan) {
        this.regSan = regSan;
    }

    public String getRegInd() {
        return regInd;
    }

    public void setRegInd(String regInd) {
        this.regInd = regInd;
    }

    public String getCommerceName() {
        return commerceName;
    }

    public void setCommerceName(String commerceName) {
        this.commerceName = commerceName;
    }

    public String getCommerceRnc() {
        return commerceRnc;
    }

    public void setCommerceRnc(String commerceRnc) {
        this.commerceRnc = commerceRnc;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCommerceAddress() {
        return commerceAddress;
    }

    public void setCommerceAddress(String commerceAddress) {
        this.commerceAddress = commerceAddress;
    }
    public String getUrl() {
        return "https://maps.google.com/?q="+lat+","+lng;
    }
}
