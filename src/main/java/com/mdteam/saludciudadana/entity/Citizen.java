package com.mdteam.saludciudadana.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel Reyes
 * Date: 08-Dec-16
 * Time: 10:14 AM
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name= "citizen")
public class Citizen {
    @Id
    private String id;
    private String name;
    private String lastName;
    private Date birthdate;
    private String birthplace;
    private String nationality;
    private String gender;
    private String maritalStatus;
    private String ocupation;

    public Citizen() {
    }
    public Citizen(String id) {
        this.id = id;
    }


    public Citizen(String id, String name, String lastName, Date birthdate, String birthplace, String nationality, String gender, String maritalStatus, String ocupation) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.birthdate = birthdate;
        this.birthplace = birthplace;
        this.nationality = nationality;
        this.gender = gender;
        this.maritalStatus = maritalStatus;
        this.ocupation = ocupation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getFullName() {
        return name + " " + lastName;
    }
}
