package com.mdteam.saludciudadana.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 09/12/2016
 * Time: 09:29 PM
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name= "product_type")
public class ProductType {

    public static ProductType MEDICO = new ProductType(1, "Medicos");
    public static ProductType ALIMENTICIO = new ProductType(2, "Alimenticios");

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    public ProductType() {
    }

    public ProductType(Integer id, String name) {
        this.name = name;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
