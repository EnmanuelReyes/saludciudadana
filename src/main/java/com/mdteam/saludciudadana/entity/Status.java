package com.mdteam.saludciudadana.entity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 11/12/2016
 * Time: 01:27 PM
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name= "status")
public class Status {

    public static final Status OPENED  = new Status(1, "Abierta");
    public static final Status IN_PROGRESS  = new Status(2, "En Progreso");
    public static final Status CLOSED  = new Status(3, "Cerrada");
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;

    public Status() {
    }

    public Status(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
