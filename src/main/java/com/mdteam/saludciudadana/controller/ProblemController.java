package com.mdteam.saludciudadana.controller;

import com.mdteam.saludciudadana.entity.Category;
import com.mdteam.saludciudadana.entity.Problem;
import com.mdteam.saludciudadana.repository.CategoryRepository;
import com.mdteam.saludciudadana.repository.ProblemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.ProblemReporter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 10/12/2016
 * Time: 02:35 PM
 */
@Controller
public class ProblemController {
    @Autowired
    ProblemRepository problemRepository;
    @GetMapping(value = "/problems/findByProductType/{id}")
    @ResponseBody
    public List<Problem> problemsList(@PathVariable Integer id){
        return problemRepository.findByProductTypeId(id);
    }
}
