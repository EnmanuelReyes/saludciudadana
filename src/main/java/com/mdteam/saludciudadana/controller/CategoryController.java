package com.mdteam.saludciudadana.controller;

import com.mdteam.saludciudadana.entity.Category;
import com.mdteam.saludciudadana.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 10/12/2016
 * Time: 01:36 PM
 */
@Controller
public class CategoryController {
    @Autowired
    CategoryRepository categoryRepository;
    @GetMapping(value = "/categories/findByProductType/{id}")
    @ResponseBody
    public List<Category> categoryList(@PathVariable Integer id){
        return categoryRepository.findByProductTypeId(id);
    }
}
