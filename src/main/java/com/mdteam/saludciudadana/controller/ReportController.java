package com.mdteam.saludciudadana.controller;

import com.mdteam.saludciudadana.entity.Report;
import com.mdteam.saludciudadana.entity.Status;
import com.mdteam.saludciudadana.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 09/12/2016
 * Time: 10:15 PM
 */
@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportRepository reportRepository;


    @GetMapping("/view")
    public ModelAndView reports(ModelAndView modelAndView) {
        modelAndView.addObject("reports", reportRepository.findAll());
        modelAndView.setViewName("reports");
        return modelAndView;
    }

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestBody Report report) {
        report.setDate(new Date());
        report.setStatus(Status.OPENED);
        reportRepository.save(report);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/edit/{reportId}")
    public ModelAndView edit(ModelAndView modelAndView, @PathVariable("reportId") Report report) {

        modelAndView.setViewName("report");
        modelAndView.addObject("report", report);
        return modelAndView;
    }


    @RequestMapping(value = "/create")
    public ModelAndView create(ModelAndView modelAndView) {

        modelAndView.setViewName("report");
        modelAndView.addObject("report", new Report());
        return modelAndView;
    }


    @RequestMapping(value = "/delete/{reportId}")
    public ModelAndView delete(ModelAndView modelAndView, @PathVariable Integer userId,
                               RedirectAttributes redirectAttributes) {
        reportRepository.delete(userId);
        redirectAttributes.addFlashAttribute("success", "El Reporte fue eliminado correctamente");
        modelAndView.setViewName("redirect:/report/view");
        return modelAndView;
    }

    @GetMapping(value = "/all")
    @ResponseBody
    public Iterable<Report> problemsList() {
        return reportRepository.findAll();
    }
}
