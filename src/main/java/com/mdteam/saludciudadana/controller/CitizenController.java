package com.mdteam.saludciudadana.controller;

import com.mdteam.saludciudadana.entity.Citizen;
import com.mdteam.saludciudadana.repository.CitizenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Enmanuel
 * Date: 11/12/2016
 * Time: 10:25 AM
 */
@Controller
public class CitizenController {
    @Autowired
    CitizenRepository citizenRepository;
    @GetMapping(value = "/citizen/{id}")
    @ResponseBody
    public Citizen problemsList(@PathVariable String id){
        return citizenRepository.findOne(id);
    }
}
